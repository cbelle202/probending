﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attach : MonoBehaviour
{
    private GameObject targetObj, player;
    private bool attached = true;

    // Start is called before the first frame update
    void Start()
    {
        attached = true;
        StartCoroutine("Disappear");
    }

    // Update is called once per frame
    void Update()
    {
        if (targetObj != null)
        {
           // Debug.Log(targetObj.transform.position);
            transform.position = targetObj.transform.position;

        }

        if (player != null && !attached)
        {
            gameObject.GetComponent<ProjectileMovement>().enabled = true;
            gameObject.GetComponent<ProjectileMovement>().SetScale(new Vector3(.5f, .2f, .2f));
            gameObject.GetComponent<Attach>().enabled = false;
        }
    }


    public void SetTargetObj(GameObject _targetObj)
    {
        targetObj = _targetObj;
    }

    public void SetPlayer(GameObject _player)
    {
        player = _player;
    }

    private IEnumerator Disappear()
    {
        yield return new WaitForSeconds(1.5f);

        Destroy(gameObject);
    }

    public void SetAttached(bool attach)
    {
        attached = attach;
    }
}
