﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBenderStateMachine : MonoBehaviour
{
    [SerializeField] private GameObject projectile;
    private GameObject projectileInst;
    [SerializeField] private GameObject leftHand;
    [SerializeField] private GameObject rightFoot;

    private Rigidbody rb;

    private int IDLE_DIR_ANIMATOR_INT = 0;
    private int LEFT_DIR_ANIMATOR_INT = -1;
    private int RIGHT_DIR_ANIMATOR_INT = 1;
    private int FWD_DIR_ANIMATOR_INT = 2;
    private int BCK_DIR_ANIMATOR_INT = -2;

    private enum AiState {MOVE, ATTACK, EVADE, DECIDE};

    private AiState aiStateEnum;
    private Vector3 _direction, _evadeDirection;
 
    private float _moveCollisionDetectionDistance = 3f;

    [SerializeField]
    GameObject destinationMarker;

    private bool isDeciding, isAttacking, isMoving, isEvading, hasEvadeMovement;


    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();

        aiStateEnum = AiState.DECIDE;

        isDeciding = false;
        isAttacking = false;
        isMoving = false;
        isEvading = hasEvadeMovement = false;

        _evadeDirection = Vector3.left;

        transform.rotation = Quaternion.Euler(0, 180, 0) ;

        _direction = Vector3.zero;
    }

    void FixedUpdate()
    {
        //Debug.Log("State: " + aiStateEnum);
        Debug.DrawRay(transform.position, _direction * _moveCollisionDetectionDistance, Color.blue);

        switch (aiStateEnum)
        {
            case AiState.MOVE:

                if(isMoving)
                {
                    Move();
                }
                else
                {
                    StartCoroutine("MoveState");
                }

                break;


            case AiState.ATTACK:
                //Debug.Log("Attack");

                if (!isAttacking)
                {
                    StartCoroutine("Attack");
                }

                break;

            case AiState.EVADE:

                if (!isEvading)
                {
                    StartCoroutine("Evade");
                }
                else if (hasEvadeMovement)
                {
                    rb.AddForce(_evadeDirection * 300, ForceMode.Force);
                }

                break;

            case AiState.DECIDE:

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 180, 0)), Time.deltaTime * 10);

                if (!isDeciding)
                {
                    gameObject.GetComponent<Animator>().SetInteger("direction", IDLE_DIR_ANIMATOR_INT);
                    StartCoroutine("Decide");
                }

                break;
        }
    }


    IEnumerator MoveState()
    {
        GetDirection();
        isMoving = true;
        yield return new WaitForSeconds(Random.Range(.75f, 1.5f));
        aiStateEnum = AiState.DECIDE;
        rb.velocity = Vector3.zero;
        _direction = Vector3.zero;
        isMoving = false;
    }







    //-------------------------------------------------------------------//
    //-------------------------EVADE--------------------------------------//
    //-------------------------------------------------------------------//
    IEnumerator Evade()
    {
        isEvading = true;
        int evadeDir = Random.Range(0, 2);

        if(evadeDir == 0)
        {
            gameObject.GetComponent<Animator>().SetTrigger("evadeLeft");
            _evadeDirection = Vector3.right;
        }
        else
        {
            gameObject.GetComponent<Animator>().SetTrigger("evadeRight");
            _evadeDirection = Vector3.left;
        }

        yield return new WaitForSeconds(2f);
        aiStateEnum = AiState.DECIDE;
        rb.velocity = Vector3.zero;
        isEvading = false;
    }

    //called by evade animation event
    void EvadeMovementTrue()
    {
        hasEvadeMovement = true;
    }

    //called by evade animation event
    void EvadeMovementFalse()
    {
        hasEvadeMovement = false;
    }
    //-------------------------------------------------------------------//
    //------END EVADE--------//
    //-------------------------------------------------------------------//







    //-------------------------------------------------------------------//
    //--------ATTACK-----------//
    //-------------------------------------------------------------------//

    IEnumerator Attack()
    {
        isAttacking = true;

        float attack = Random.Range(0f, 1f);

        if (attack <= 0.5f)
        {
            gameObject.GetComponent<Animator>().SetTrigger("kick");
        }
        else
        {
            gameObject.GetComponent<Animator>().SetTrigger("jab");
        }

        yield return new WaitForSeconds(Random.Range(1f, 2f)); //change to wait for attack animation to end
        aiStateEnum = AiState.DECIDE;
        isAttacking = false;
    }

    //called by attack animation event
    void JabPrep()
    {
        //Debug.Log("JabPrep");
        projectileInst = Instantiate(projectile, leftHand.transform.position, Quaternion.identity);
        projectileInst.GetComponent<Attach>().SetTargetObj(leftHand);
        projectileInst.GetComponent<Attach>().SetPlayer(gameObject);
        projectileInst.GetComponent<ProjectileMovement>().enabled = false;
        projectileInst.GetComponent<ProjectileMovement>().SetRotation(transform);

    }

    //called by attack animation event
    void KickPrep()
    {
        projectileInst = Instantiate(projectile, rightFoot.transform.position, Quaternion.identity);
        projectileInst.GetComponent<Attach>().SetTargetObj(rightFoot);
        projectileInst.GetComponent<Attach>().SetPlayer(gameObject);
        projectileInst.GetComponent<ProjectileMovement>().enabled = false;
        projectileInst.GetComponent<ProjectileMovement>().SetRotation(transform);

    }

    //called by attack animation event
    void ElementReleaseTrue()
    {
        //release = true;
        projectileInst.GetComponent<Attach>().SetAttached(true);
    }

    //called by attack animation event
    void ElementReleaseFalse()
    {
        //release = false;
        projectileInst.GetComponent<Attach>().SetAttached(false);
    }
    //-------------------------------------------------------------------//
    //-------------------------------------------------------------------//
    //----------END ATTACK--------------//
    //-------------------------------------------------------------------//
    //-------------------------------------------------------------------//






    IEnumerator Decide()
    {
        isDeciding = true;
        yield return new WaitForSeconds(Random.Range(0f, 2.5f));

        float nextState = Random.Range(0, 3);

        // Debug.Log("next state: " + nextState);

        switch (nextState)
        { 
            case 0:
                aiStateEnum = AiState.MOVE;
                break;

            case 1:
                aiStateEnum = AiState.ATTACK;
                break;

            case 2:
                aiStateEnum = AiState.EVADE;
                break;
        }
        
        isDeciding = false;
    }


    private void GetDirection()
    {
        Vector3 randomDirection = Vector3.zero;
        int totalAttempts = 20;
        int numAttempts = 0;

        do
        {
            numAttempts++;
            randomDirection = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized;
            Debug.DrawRay(transform.position, randomDirection * _moveCollisionDetectionDistance, Color.green);
            //Debug.Log("random dir: " + randomDirection);

        } while (numAttempts < totalAttempts && IsPathBlocked(randomDirection));

        _direction = randomDirection;
    }

    private bool IsPathBlocked(Vector3 direction)
    {
        Ray ray = new Ray(transform.position, direction);
        //Debug.Log("ray: " + ray);
        RaycastHit hit = new RaycastHit();

        Physics.Raycast(ray, out hit, _moveCollisionDetectionDistance);


        //Debug.Log("hit: " + hit.collider);

        return hit.collider != null;
    }


    private void Move()
    {
        float angle = Vector3.SignedAngle(Vector3.forward, _direction, Vector3.up);

        Quaternion rotation = Quaternion.Euler(new Vector3(0, angle, 0));

        const float minBack = 300, maxBack = 60;
        const float maxFwd = 225, minFwd = 135;

        if ((rotation.eulerAngles.y >= minBack && rotation.eulerAngles.y <= 360) ||
            (rotation.eulerAngles.y >= 0 && rotation.eulerAngles.y <= maxBack))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, rotation.eulerAngles.y + 180, 0)), Time.deltaTime * 10);
            gameObject.GetComponent<Animator>().SetInteger("direction", BCK_DIR_ANIMATOR_INT);
        }
        else if (rotation.eulerAngles.y >= maxFwd && rotation.eulerAngles.y < minBack)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, rotation.eulerAngles.y, 0)), Time.deltaTime * 10);
            gameObject.GetComponent<Animator>().SetInteger("direction", RIGHT_DIR_ANIMATOR_INT);
        }
        else if (rotation.eulerAngles.y > maxBack && rotation.eulerAngles.y <= minFwd)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, rotation.eulerAngles.y, 0)), Time.deltaTime * 10);
            gameObject.GetComponent<Animator>().SetInteger("direction", LEFT_DIR_ANIMATOR_INT);
        }
        else if (rotation.eulerAngles.y > minFwd && rotation.eulerAngles.y < maxFwd)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, rotation.eulerAngles.y, 0)), Time.deltaTime * 10);
            gameObject.GetComponent<Animator>().SetInteger("direction", FWD_DIR_ANIMATOR_INT);
        }

        rb.AddForce(_direction * 200, ForceMode.Force);
    }

}
