﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    private float speed = 10f;
    private Transform targetTransform;
    private GameObject targetObj;
    private bool move = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Disappear");
        //transform.rotation = Quaternion.Euler(new Vector3(-90f, 0, 0));

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(0, 0, 1) * speed * Time.deltaTime);

    }

    private IEnumerator Disappear()
    {
        yield return new WaitForSeconds(4f);

        Destroy(gameObject);
    }

    public void SetDirection(float degrees)
    {
        transform.Rotate(0f, degrees, 0f, Space.Self);

       // Debug.Log(dir.y);
    }

    public void SetTargetObj(GameObject _targetObj)
    {
        targetObj = _targetObj;
    }
   

    public void SetTransform(Transform _targetTransform)
    {
        transform.SetParent(_targetTransform);

        transform.localRotation = Quaternion.Euler(new Vector3(-90f, transform.rotation.y, transform.rotation.z));
    }


    public void SetScale(Vector3 scale)
    {
        //transform.localScale = scale; //Vector3.Lerp(transform.localScale, scale, Time.deltaTime * 10); //scale;
    }

    public void SetRotation(Transform benderTransform)
    {
        transform.rotation = benderTransform.rotation;
    }

}
