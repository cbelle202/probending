using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    GameObject[] opponents = new GameObject[3];

    [SerializeField]
    GameObject[] opponentZone1StartPositions = new GameObject[3];

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < opponents.Length; i++)
        {
            opponents[i] = Instantiate(opponents[i], opponentZone1StartPositions[i].transform.position, Quaternion.Euler(0, 180, 0));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject[] GetOpponents()
    {
        return opponents;
    }
}
