﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour
{
    [SerializeField] private Transform route;
    [SerializeField] private float tparam = .5f; //modified when player moves left or right. Used for position in curve/route
    [SerializeField] private float speed = .002f;

    private int dir = 1;

    // Start is called before the first frame update
    void Start()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < -6)
        {
            dir = -1;
        }
        else if (transform.position.x > 6)
        {
            dir = 1;
        }

        tparam += speed * dir;

        GoAlongRoute();
        
    }


    private void GoAlongRoute()
    {
        //control points
        Vector3 p1 = route.GetChild(0).position;
        Vector3 p2 = route.GetChild(1).position;
        Vector3 p3 = route.GetChild(2).position;
        Vector3 p4 = route.GetChild(3).position;

        transform.position = Mathf.Pow(1 - tparam, 3) * p1 +
                3 * Mathf.Pow(1 - tparam, 2) * tparam * p2 +
                3 * (1 - tparam) * Mathf.Pow(tparam, 2) * p3 +
                Mathf.Pow(tparam, 3) * p4;
    }


    
}
