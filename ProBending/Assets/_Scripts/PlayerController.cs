﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject projectile;
    private GameObject projectileInst;
    [SerializeField] private GameObject leftHand;
    [SerializeField] private GameObject rightFoot;

    //opponent targeting
    [SerializeField] private GameObject gameController;
    [SerializeField] private GameObject targetMarker;
    [SerializeField] private GameObject target;
    private int targetIndex = 0;
    private Vector3 targetPos;

    [SerializeField] private float tparam = .5f; //modified when player moves left or right. Used for position in curve/route
    [SerializeField] private float speed = 6;
    [SerializeField] private Animator animator;

    private string KICK_ANIMATOR_TRIGGER = "kick";
    private string JAB_ANIMATOR_TRIGGER = "jab";
    private int IDLE_DIR_ANIMATOR_INT = 0;
    private int LEFT_DIR_ANIMATOR_INT = -1;
    private int RIGHT_DIR_ANIMATOR_INT = 1;
    private int FWD_DIR_ANIMATOR_INT = 2;
    private int BCK_DIR_ANIMATOR_INT = -2;

    private int walkDirection = 0;

    private bool release = false;

    // Start is called before the first frame update
    void Start()
    {
            if (gameController.GetComponent<GameController>().GetOpponents()[0] != null)
            {
            
                target = gameController.GetComponent<GameController>().GetOpponents()[targetIndex];
                targetMarker = Instantiate(targetMarker, new Vector3(target.transform.position.x, target.transform.position.y + 2, target.transform.position.z), Quaternion.identity);
            }
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInput();
        //Debug.DrawRay(transform.position, transform.rotation * Vector3.forward, Color.green);
        targetPos = target.transform.position;

        targetMarker.transform.position = new Vector3(targetPos.x, targetPos.y + 2, targetPos.z);
    }


    private void ProcessInput()
    {
        if (Input.GetKey(KeyCode.D))    //Move or Aim Right
        {
            MoveRight();
        }
        else if (Input.GetKey(KeyCode.A))   //Move or Aim Left
        {
            MoveLeft();

        }
        else if (Input.GetKey(KeyCode.S))   //Move Backwards
        {
            MoveBackward();

        }
        else if (Input.GetKey(KeyCode.W))   //Move Forward
        {
            MoveForward();

        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))   //Jab
        {
            animator.SetTrigger("jab");
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))   //Kick
        {
            animator.SetTrigger("kick");
        }
        else if (Input.GetKeyDown(KeyCode.Tab))   //switch target
        {
            if(targetIndex >= gameController.GetComponent<GameController>().GetOpponents().Length-1)
            {
                targetIndex = 0;
            }
            else
            {
                targetIndex++;
            }
            target = gameController.GetComponent<GameController>().GetOpponents()[targetIndex];
        }
        else
        {

            animator.SetInteger("direction", IDLE_DIR_ANIMATOR_INT);
            
            LookAtTarget();
        }
    }


    private void LookAtTarget()
    {
        
        float angle = Vector3.SignedAngle(Vector3.forward, targetPos - transform.position, Vector3.up);
        Quaternion rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, angle, 0)), Time.deltaTime * 10);
        transform.rotation = rotation;
        
    }




    private void MoveForward()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, 0)), Time.deltaTime * 10);
        transform.position += speed * Vector3.forward * Time.deltaTime;

        animator.SetInteger("direction", FWD_DIR_ANIMATOR_INT);
    }


    private void MoveBackward()
    {

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, 0)), Time.deltaTime * 10);
        transform.position += speed * Vector3.back * Time.deltaTime;

        animator.SetInteger("direction", BCK_DIR_ANIMATOR_INT);
    }



    private void MoveRight()
    {

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 90, 0)), Time.deltaTime * 10);
        transform.position += speed * Vector3.right * Time.deltaTime;

        animator.SetInteger("direction", RIGHT_DIR_ANIMATOR_INT);
    }


    private void MoveLeft()
    {
 
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, -90, 0)), Time.deltaTime * 10);
        transform.position += speed * Vector3.left * Time.deltaTime;

        animator.SetInteger("direction", LEFT_DIR_ANIMATOR_INT);
    }




    void JabPrep()
    {
        Debug.Log("JabPrep");
        projectileInst = Instantiate(projectile, leftHand.transform.position, Quaternion.identity);
        projectileInst.GetComponent<Attach>().SetTargetObj(leftHand);
        projectileInst.GetComponent<Attach>().SetPlayer(gameObject);
        projectileInst.GetComponent<ProjectileMovement>().SetDirection(transform.eulerAngles.y);
        projectileInst.GetComponent<ProjectileMovement>().enabled = false;
    }


    void KickPrep()
    {
        Debug.Log("KickPrep");
        projectileInst = Instantiate(projectile, rightFoot.transform.position, Quaternion.identity);
        projectileInst.GetComponent<Attach>().SetTargetObj(rightFoot);
        projectileInst.GetComponent<Attach>().SetPlayer(gameObject);
        projectileInst.GetComponent<ProjectileMovement>().SetDirection(transform.eulerAngles.y);
        projectileInst.GetComponent<ProjectileMovement>().enabled = false;
    }

    void ElementReleaseTrue()
    {
        //release = true;
        projectileInst.GetComponent<Attach>().SetAttached(true);
    }

    void ElementReleaseFalse()
    {
        //release = false;
        projectileInst.GetComponent<Attach>().SetAttached(false);
    }


    //called by evade animation event
    void EvadeMovementTrue()
    {
       // hasEvadeMovement = true;
    }

    //called by evade animation event
    void EvadeMovementFalse()
    {
       // hasEvadeMovement = false;
    }
}
